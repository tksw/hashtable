CC = gcc
CFLAGS = -Wall -O2
PROG = main
LDLIBS = -lm

SRC_DIR = ./src
BIN_DIR = ./bin

ALL_SRC_FILES = $(wildcard $(SRC_DIR)/*.c)

compile:
	$(CC) $(CFLAGS) $(ALL_SRC_FILES) -o $(BIN_DIR)/$(PROG) $(LDLIBS)

